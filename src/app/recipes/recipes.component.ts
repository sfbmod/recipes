import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  recipeObj: Recipe = {
    Id: 1,
    Name: "Creamy White Chili with Cream Cheese",
    PictureUrl: "http://images.media-allrecipes.com/userphotos/960x960/6183594.jpg",
    Notes: null,
    Ingredient1: 1,
    Ingredient2: 2,
    Ingredient3: 3
  };

  constructor() {  }

  ngOnInit() {
  }

}
