export class Recipe {
  Id: number;
  Name: string;
  PictureUrl: string;
  Notes: string[]
  Ingredient1: number;
  Ingredient2: number;
  Ingredient3: number;
}
